<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\BookmarkController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'logIn']);
Route::get('profile/{id}', [UserController::class, 'profile']);
Route::post('update-profile/{id}', [UserController::class, 'updateProfile']);
Route::post('add-course', [CourseController::class, 'add']);
Route::get('courses', [CourseController::class, 'get']);
Route::get('course/{id}', [CourseController::class, 'getById']);
Route::post('bookmark/{userId}/{courseId}', [BookmarkController::class, 'add']);
Route::get('bookmark/{userId}', [BookmarkController::class, 'get']);
Route::post('remove-bookmark/{id}', [BookmarkController::class, 'remove']);
