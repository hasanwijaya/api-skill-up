<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(Request $request) {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'phone_number' => 'required',
            'password' => 'required',
        ]);
  
        $status = "error";
        $message = "";
        $data = null;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $user = \App\Models\User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'password' => \Hash::make($request->password)
            ]);

            if ($user) {
                $status = "success";
                $message = "register successfully";
                $data = $user;
            } else {
                $message = 'register failed';
            }
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function logIn(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
  
        $user = \App\Models\User::where('email', $request->email)->first();
        $status = "error";
        $message = "";
        $data = null;
  
        if ($user) {
          if (\Hash::check($request->password, $user->password)) {
            $status = 'success';
            $message = 'Login successfully';
            $data = $user;
          } else {
            $message = "Login failed, password salah";
          }
        } else {
          $message = "Login failed, user " . $request->email . " tidak ditemukan";
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function profile(Request $request)
    {
      $user = \App\Models\User::findOrFail($request->id);

      return response()->json($user);
    }

    public function updateProfile(Request $request)
    {
      $user = \App\Models\User::findOrFail($request->id);
      $user->name = $request->name;
      $user->email = $request->email;
      $user->phone_number = $request->phone_number;
      $user->save();

      return response()->json([
        'status' => 'success',
        'message' => 'Update successfully'
      ]);
    }
}
