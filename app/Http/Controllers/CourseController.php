<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function add(Request $request) {
        $course = new \App\Models\Course;
        $course->nama = $request->nama;
        $course->deskripsi = $request->deskripsi;
        $course->level = $request->level;
        $course->gambar = $request->gambar->store('gambar', 'public');
        $course->save();

        return response()->json(['status' => 'success']);
    }

    public function get(Request $request) {
        $courses = \App\Models\Course::all();

        return response()->json($courses);
    }

    public function getById($id) {
        $course = \App\Models\Course::findOrFail($id);

        return response()->json($course);
    }
}
