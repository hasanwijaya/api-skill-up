<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    public function add($userId, $courseId) {
        $user = \App\Models\User::findOrFail($userId);
        $user->bookmarks()->create(['course_id' => $courseId]);

        return response()->json(['status' => 'success']);
    }

    public function get($userId) {
        $user = \App\Models\User::findOrFail($userId);

        return response()->json($user->bookmarks);
    }

    public function remove($id) {
        $bookmark = \App\Models\Bookmark::findOrFail($id);
        $bookmark->delete();

        return response()->json(['status' => 'success']);
    }
}
